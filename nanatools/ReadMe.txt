nanatools - (c) 2016 Shaun "neko68k" Thompson

This is a simple tool that allows extracting and replacing textures in NanaOn-Sha's
Playstation games PaRappa the Rapper and UmJammer Lammy. Usage is simple.

Usage: nanatools <mode> <source.INT> (OLDFILE.TIM) (NEWFILE.TIM)
	<mode>:
	-l list
		List all files in TOC
	-e extract
		Extracts all files to their own directory.
	-r insert
		(OLDFILE.TIM) is the name of the original file to replace.
		(NEWFILE.TIM) can be any image in 4-bit TIM format.