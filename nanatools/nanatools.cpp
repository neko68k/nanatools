// nanatools.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <direct.h>
#include <string.h>

typedef struct INT{
	uint32_t tocnum;
	uint32_t numfiles;
	uint32_t blocksize;
	uint32_t zero;
};

typedef struct TOC{
		uint32_t size;
		char fn[16];
};

const char *usagestring = "nanatools - (c) 2016 Shaun \"neko68k\" Thompson\n\nUsage: nanatools <mode> <source.INT> (OLDFILE.TIM) (NEWFILE.TIM)\n\t<mode>:\n\t-l list\n\t\tList all files in TOC\n\t-e extract\n\t\tExtracts all files to their own directory.\n\t-r insert\n\t\t(OLDFILE.TIM) is the name of the original file to replace.\n\t\t(NEWFILE.TIM) can be any image in 4-bit TIM format.\n";

int list(char *fn){
	INT inthdr;
	TOC *toc=NULL;
	FILE *in=NULL;
	uint32_t i = 0;

	in = fopen(fn, "rb");

	if(!in){
		printf("File not found: %s\n", fn);
		return 1;
	}

	printf("Listing TOC 1\n\n");
	fread(&inthdr, sizeof(INT), 1, in);
	toc = (TOC*)malloc(sizeof(TOC)*inthdr.numfiles);
	fread(toc, sizeof(TOC), inthdr.numfiles, in);

	for(i=0;i<inthdr.numfiles;i++){
		printf("%s\n", toc[i].fn);
	}

	i = 0;
	free(toc);
	fseek(in, inthdr.blocksize*2048+0x2000, SEEK_SET);
	fread(&inthdr, sizeof(INT), 1, in);
	toc = (TOC*)malloc(sizeof(TOC)*inthdr.numfiles);
	fread(toc, sizeof(TOC), inthdr.numfiles, in);

	printf("\n\nListing TOC 2\n\n");
	for(i=0;i<inthdr.numfiles;i++){
		printf("%s\n", toc[i].fn);
	}

	fclose(in);
	free(toc);
}

int replace(char *fn, char *repfn, char *imgfn){
	INT inthdr;
	TOC *toc=NULL;
	FILE *in=NULL;
	uint32_t i = 0;
	uint32_t tocloc=0;
	uint8_t *newImg;
	FILE *newImgFile;
	uint32_t writeofs = 0x2000;

	in = fopen(fn, "rb+");

	if(!in){
		printf("File not found: %s\n", fn);
		return 1;
	}

	newImgFile = fopen(imgfn, "rb");

	if(!newImgFile){
		printf("File not found: %s\n", fn);
		return 1;
	}

	fread(&inthdr, sizeof(INT), 1, in);
	toc = (TOC*)malloc(sizeof(TOC)*inthdr.numfiles);
	fread(toc, sizeof(TOC), inthdr.numfiles, in);

	printf("Searching...\n");
	
	while(strcmpi(toc[i].fn, repfn) && i<inthdr.numfiles){
		writeofs += toc[i].size;
		i++;
	}

	if(i==inthdr.numfiles){
		// do second TOC here
		i = 0;
		writeofs = inthdr.blocksize*2048+0x4000;
		fseek(in, writeofs-0x2000, SEEK_SET);
		fread(&inthdr, sizeof(INT), 1, in);
		free(toc);
		toc = (TOC*)malloc(sizeof(TOC)*inthdr.numfiles);
		fread(toc, sizeof(TOC), inthdr.numfiles, in);

		while(strcmpi(toc[i].fn, repfn) && i<inthdr.numfiles){
			writeofs += toc[i].size;
			i++;
		}
		if(i==inthdr.numfiles){
			free(toc);
			fclose(in);
			printf("Original file %s not found in TOC\n", repfn);
			return 1;
		}
	}

	printf("Found %s\n", repfn);

	newImg = (uint8_t*)malloc(toc[i].size);
	fread(newImg, toc[i].size, 1, newImgFile);
	fclose(newImgFile);

	fseek(in, writeofs, SEEK_SET);
	fwrite(newImg, toc[i].size, 1, in);
	fclose(in);
	
	free(newImg);
	free(toc);

	printf("Success!\n");

	return 0;
}

int dump(char *fn){
	INT inthdr;
	TOC *toc=NULL;
	FILE *in=NULL;
	FILE *out=NULL;
	uint8_t *buf=NULL;
	uint32_t i = 0;
	uint32_t readofs=0;
	char outdir[_MAX_PATH];
	uint32_t filesize;
	uint32_t tocloc=0;

	in = fopen(fn, "rb");
	if(!in){
		printf("File not found: %s", fn);
		return 1;
	}

	sprintf(outdir, "%s.ripped", fn);
	mkdir(outdir);
	chdir(outdir);

	fseek(in, 0, SEEK_END);
	filesize = ftell(in);
	fseek(in, 0, SEEK_SET);

	fread(&inthdr, sizeof(INT), 1, in);

	buf = (uint8_t*)malloc(inthdr.blocksize*2048);
	toc = (TOC*)malloc(sizeof(TOC)*inthdr.numfiles);

	fread(toc, sizeof(TOC), inthdr.numfiles, in);
	fseek(in, 0x2000, SEEK_SET);
	fread(buf, inthdr.blocksize*2048, 1, in);
	
	for(i = 0;i<inthdr.numfiles;i++){
		out = fopen(toc[i].fn, "wb");
		printf("%s\n", toc[i].fn);
		fwrite(&buf[readofs], toc[i].size, 1, out);
		fclose(out);
		readofs += toc[i].size;
	}

	// second TOC

	readofs = 0;

	free(toc);
	free(buf);

	tocloc = ftell(in);
	fread(&inthdr, sizeof(INT), 1, in);

	buf = (uint8_t*)malloc(inthdr.blocksize*2048);
	toc = (TOC*)malloc(sizeof(TOC)*inthdr.numfiles);

	fread(toc, sizeof(TOC), inthdr.numfiles, in);
	fseek(in, 0x2000+tocloc, SEEK_SET);
	fread(buf, inthdr.blocksize*2048, 1, in);
	
	for(i = 0;i<inthdr.numfiles;i++){
		out = fopen(toc[i].fn, "wb");
		printf("%s\n", toc[i].fn);
		fwrite(&buf[readofs], toc[i].size, 1, out);
		fclose(out);
		readofs += toc[i].size;
	}

	free(toc);
	free(buf);

	fclose(in);
	return 0;
}

int main(int argc, char* argv[]){
	system("pause");
	if(argc>1){
		if(!strcmpi("-r", argv[1])){
			if(argc<5){
				printf(usagestring);
				return 1;
			} else {
				replace(argv[2], argv[3], argv[4]);
			}
		} else if(!strcmpi("-e", argv[1])){
			if(argc<3){
				printf(usagestring);
				return 1;
			} else {
				dump(argv[2]);
			}
		}
		else if(!strcmpi("-l", argv[1])){
			if(argc<3){
				printf(usagestring);
				return 1;
			} else {
				list(argv[2]);
			}
		} else {
			printf(usagestring);
			return 1;
		}
	} else {
		printf(usagestring);
		return 1;
	}
	return 0;
}

